<?php
/**
 * @file
 * compro_antispam_unmollom.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function compro_antispam_unmollom_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
